import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Loader from 'react-loader-spinner';
import OwlCarousel from 'react-owl-carousel2';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getPoetList } from '../../actions/poetActions';
import placeHolder from '../../media/artist-placeHolder.jpg';

class HomeArtists extends Component {
  /**
   * adding isLoading state to constructor
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }

  static propTypes = {
    light: PropTypes.bool,
    getPoetList: PropTypes.func.isRequired,
    poets: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  };

  /**
   * OwlCarousel options
   * @see OwlCarousel
   */
  options = {
    loop: false,
    margin: 30,
    nav: true,
    navText: ['', ''],
    dotsEach: true,
    autoplay: true,
    autoplayHoverPause: true,
    rewind: true,
    startPosition: 1,
    responsive: {
      0: {
        items: 1,
        stagePadding: 0
      },
      400: {
        items: 3,
        stagePadding: 0
      },
      700: {
        items: 5,
        stagePadding: 0
      },
      1200: {
        items: 7,
        stagePadding: 30
      },
      1600: {
        items: 10,
        stagePadding: 30
      }
    }
  };

  /**
   * Checks if new page is loaded and sets loading state to false
   * @param {*} nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.poets.page &&
      nextProps.poets.page !== this.props.poets.page
    ) {
      this.setState({ isLoading: false });
    }
  }

  /**
   * processes artists array and returs OwlCarousel JSX elements
   * @param {array} artistsArr unprocesses artists array
   * @returns {array} JSX OwlCarousel elements
   */
  processItems = artistsArr => {
    return artistsArr.map(element => {
      return (
        <div
          className="item gallery-item gallery-style-10 text-right padding"
          key={element._id}
        >
          <Link to={`/poet/${element._id}`}>
            <img
              src={element.image ? element.image : placeHolder}
              alt=""
              className="item-img"
            />
          </Link>
          <div className="item-title">
            <p>
              <strong>{element.name}</strong>
            </p>
          </div>
        </div>
      );
    });
  };

  /**
   * checks if poets are loaded, if not will send a redux action to load them
   */
  componentDidMount() {
    if (!this.props.poets.page) {
      this.loadContent(1);
    } else {
      this.setState({ isLoading: false });
    }
  }

  /**
   * will load page and sets isLoading state to true
   * @param {number} page page number to load
   */
  loadContent = page => {
    this.setState({ isLoading: true });
    this.props.getPoetList(page);
  };

  render() {
    const { docs: poets, page, pages } = this.props.poets;
    return (
      <section
        id="gallery-half-carousel"
        className={classNames(
          'section-carousel pt-100 pb-100 gallery carousel-nav-left-bottom carousel-dots-none overall',
          {
            dark: !this.props.light,
            light: !!this.props.light
          }
        )}
      >
        <div className="container-fluid offset-left">
          <div className="row">
            <div className="col-12">
              <div className="row col-12">
                <h3>
                  <strong>{this.props.t('homeArtists.title')}</strong>
                </h3>
                {!!page && page < pages && (
                  <span
                    onClick={this.loadContent.bind(this, page + 1)}
                    className="small padding"
                    style={{ cursor: 'pointer' }}
                  >
                    {this.props.t('homeArtists.nextPage')}
                  </span>
                )}
                {!!page && page > 1 && (
                  <span
                    onClick={this.loadContent.bind(this, page - 1)}
                    className="small padding"
                    style={{ cursor: 'pointer' }}
                  >
                    {this.props.t('homeArtists.prevPage')}
                  </span>
                )}
              </div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height={20}
                viewBox="0 0 100 20"
                width={100}
                className="mb-50 svg-primary"
              >
                <path d="m0 9h100v2h-100z" fillRule="evenodd" />
              </svg>
            </div>
            {this.state.isLoading ? (
              <div className="loader">
                <Loader type="ThreeDots" color="#333" height={80} width={80} />
              </div>
            ) : (
              <OwlCarousel ref="poets" options={this.options}>
                {this.processItems(poets)}
              </OwlCarousel>
            )}
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  poets: state.poet.poetList
});

export default connect(
  mapStateToProps,
  { getPoetList }
)(HomeArtists);
