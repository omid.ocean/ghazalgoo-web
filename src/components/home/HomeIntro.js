import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

const HomeIntro = props => {
  return (
    <section
      id="desc-text-halfbg-3"
      className={classNames('pt-75 pt-md-150 pb-md-150', {
        dark: !props.light,
        light: !!props.light
      })}
    >
      <div className="container">
        <div className="row">
          <div className="col-md-7">
            <h2>
              <strong>{props.t('homeIntro.title')}</strong>
            </h2>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height={20}
              viewBox="0 0 40 20"
              width={40}
              className="mb-30 svg-secondary"
            >
              <path d="m0 8h40v4h-40z" fillRule="evenodd" />
            </svg>
            <p className="mb-50">{props.t('homeIntro.desc')}</p>
            <Link className="btn btn-secondary" to="/about-us">
              {props.t('homeIntro.button')}
            </Link>
          </div>
          <div
            id="desc-img"
            className="bg-box col-md-6 mt-50 mt-md-0 embed-responsive-4by3"
          />
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </div>
    </section>
  );
};

HomeIntro.propTypes = {
  t: PropTypes.func.isRequired,
  light: PropTypes.bool
};

export default HomeIntro;
