import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { queryPoems } from '../../actions/poemActions';
import authorPlaceHolder from '../../media/artist-placeHolder.jpg';
import { DebounceInput } from 'react-debounce-input';

class HomeHeader extends Component {
  state = {
    query: ''
  };

  handleInputChange = event => {
    this.setState({
      query: event.target.value
    });
    this.props.queryPoems(this.state.query, 1);
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.queryPoems(this.state.query, 1);
  };

  resultsetHandler = () => {
    if (this.props.queryResults.total && this.props.queryResults.total > 0) {
      const { docs: poems } = this.props.queryResults;
      return (
        <ul className="query-res">
          {poems.map(el => (
            <li key={el._id}>
              <Link to={`/poem/${el._id}`}>
                <img
                  width={50}
                  height={50}
                  alt="author"
                  className="rounded-circle"
                  src={el.poet.image ? el.poet.image : authorPlaceHolder}
                />
                <h5>{el.poemName}</h5>
                <p>{el.poet.name}</p>
              </Link>
            </li>
          ))}
        </ul>
      );
    }
  };

  static propTypes = {
    t: PropTypes.func.isRequired,
    light: PropTypes.bool,
    queryResults: PropTypes.object.isRequired,
    queryPoems: PropTypes.func.isRequired
  };

  render() {
    return (
      <header
        id="header-search"
        className={classNames('pt-75 pb-75 pt-md-150 pb-md-150', {
          dark: !this.props.light,
          light: !!this.props.light
        })}
      >
        <div className="container">
          <div className="row">
            <div className="col-md-12 ml-auto mr-auto">
              <h3 className="mb-30">
                <strong>{this.props.t('homeHeader.title')}</strong>
              </h3>
              <form
                onSubmit={this.onSubmit}
                className="contact_form form-inline mb-20 shadow bg-default light"
                id="header-search-form"
                noValidate="novalidate"
              >
                <div className="form-group email-field-group">
                  <DebounceInput
                    type="text"
                    className="form-control"
                    placeholder={this.props.t('homeHeader.input')}
                    minLength={4}
                    debounceTimeout={500}
                    onChange={this.handleInputChange}
                  />
                  {this.resultsetHandler()}
                </div>
                <button type="submit" className="btn btn-link light">
                  <FontAwesomeIcon
                    icon="search"
                    className="icon svg-secondary icon-pos-left"
                    viewBox="0 0 400 400"
                  />
                  <strong>{this.props.t('homeHeader.search')}</strong>
                </button>
              </form>
              <p>
                {this.props.t('homeHeader.popular')}:{' '}
                <Link to="/poet/5bfba256748ac01dcc04586d">حافظ</Link> ،
                <Link to="/poet/5bfba256748ac01dcc045872">سعدی</Link> ،
                <Link to="/poet/5bfba256748ac01dcc04586f">فردوسی</Link>
              </p>
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </header>
    );
  }
}

const mapStateToProps = state => ({
  queryResults: state.poem.queryResults
});

export default connect(
  mapStateToProps,
  { queryPoems }
)(HomeHeader);
