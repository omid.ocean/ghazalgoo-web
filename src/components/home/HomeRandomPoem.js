import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Loader from 'react-loader-spinner';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getRandomPoem } from '../../actions/poemActions';
import { displayVerses } from '../../utils/poemTools';

/**
 * class Representing Random Poem, displayed in HomePage
 * @extends PureComponent
 */
class HomeRandomPoem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }
  /** React Proptypes
   * @see PropTypes
   */
  static propTypes = {
    stars: PropTypes.number.isRequired,
    light: PropTypes.bool,
    getRandomPoem: PropTypes.func.isRequired,
    poem: PropTypes.object.isRequired
  };
  /**
   * displays stars using @see FontAwesomeIcon
   * @param {number} stars - number of stars to be displayed
   * @return {JSX} - jsx element representing stars
   */
  starCount = stars => {
    const starsArr = [];
    for (let i = 0; i < stars; i++) {
      starsArr.push(
        <FontAwesomeIcon
          icon="star"
          viewBox="0 0 400 400"
          className="icon svg-primary"
          key={i}
        />
      );
    }
    return starsArr;
  };

  /**
   * will send a REDUX action to get a random poem
   */
  componentDidMount() {
    if (!this.props.poemName) {
      this.props.getRandomPoem();
    } else {
      this.setState({ isLoading: false });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.poem.poemName) {
      this.setState({ isLoading: false });
    }
  }
  render() {
    /**
     * destructering poem from react props
     */
    const { poem } = this.props;
    return (
      <section
        id="testimonial-single"
        className={classNames('pt-100 pb-100 text-center', {
          dark: !this.props.light,
          light: !!this.props.light
        })}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 ml-auto mr-auto">
              {this.state.isLoading ? (
                <div className="loader">
                  <Loader
                    type="ThreeDots"
                    color="#fff"
                    height={80}
                    width={80}
                  />
                </div>
              ) : (
                <div className="content-box blockquote">
                  <div className="inline-group">
                    {this.starCount(this.props.stars)}
                  </div>
                  {displayVerses(poem.verses, 4, true)}
                  <Link
                    style={{ marginTop: '1.4em', display: 'block' }}
                    to={`/poem/${poem._id}`}
                    className="text-secondary blockquote-footer"
                  >
                    {poem.poemName}
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}
/**
 * maps redux state to props
 * @param {*} state
 */
const mapStateToProps = state => ({
  poem: state.poem.randomPoem
});

export default connect(
  mapStateToProps,
  { getRandomPoem }
)(HomeRandomPoem);
