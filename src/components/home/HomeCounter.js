import PropTypes from 'prop-types';
import React from 'react';
import CountUp from 'react-countup';

const HomeCounter = props => {
  return (
    <section
      id="benefits-3col-counter-3"
      className="counter-up text-center dark"
    >
      <div className="container-fluid">
        <div className="row no-gutters">
          <div className="col-md-4 d-flex flex-column flex-equal-items">
            <div
              className="content-box padding-x3"
              style={{ backgroundColor: '#444' }}
            >
              <h2 className="count-up-data">
                <CountUp prefix="+ " end={props.poemCount} duration={5} />
              </h2>
              <h4>{props.t('homeCounter.poems')}</h4>
            </div>
          </div>
          <div className="col-md-4 d-flex flex-column flex-equal-items">
            <div
              className="content-box padding-x3"
              style={{ backgroundColor: '#EE5B4F' }}
            >
              <h2 className="count-up-data">
                <CountUp prefix="+ " end={props.poetCount} duration={5} />
              </h2>
              <h4>{props.t('homeCounter.poets')}</h4>
            </div>
          </div>
          <div className="col-md-4 d-flex flex-column flex-equal-items">
            <div
              className="content-box padding-x3"
              style={{ backgroundColor: '#68BACD' }}
            >
              <h2 className="count-up-data">
                <CountUp prefix="+ " end={props.verseCount} duration={5} />
              </h2>
              <h4>{props.t('homeCounter.verses')}</h4>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-wrap">
        <div className="bg" />
      </div>
    </section>
  );
};

HomeCounter.propTypes = {
  poemCount: PropTypes.number.isRequired,
  poetCount: PropTypes.number.isRequired,
  verseCount: PropTypes.number.isRequired,
  t: PropTypes.func.isRequired
};

export default HomeCounter;
