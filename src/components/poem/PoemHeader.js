import React, { Component } from 'react';
import PropTypes from 'prop-types';
import placeHolder from '../../media/artist-placeHolder.jpg';
import { Link } from 'react-router-dom';
class PoemHeader extends Component {
  static propTypes = {
    poetName: PropTypes.string.isRequired,
    poet: PropTypes.object.isRequired
  };

  render() {
    const { poemName, poet } = this.props;
    if (!poemName) return <p>Loading...</p>;
    return (
      <header
        id="header-text-7"
        className="pt-75 pb-75 pt-md-150 pb-md-150 pt-lg-250 pb-lg-250 text-center light"
      >
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-10 ml-auto mr-auto">
              <h2 className="mb-20">
                <strong>{poemName}</strong>
              </h2>
              <img
                className="rounded-circle"
                src={poet.image ? poet.image : placeHolder}
                alt="artist profile"
                width="50px"
              />
              <p className="blockquote-footer d-inline-block">
                <Link to={`/poet/${poet._id}`}>{poet.name}</Link>
              </p>
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </header>
    );
  }
}

export default PoemHeader;
