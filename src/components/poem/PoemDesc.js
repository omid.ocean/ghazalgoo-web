import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { displayVerses } from '../../utils/poemTools';

class PoemDesc extends Component {
  static propTypes = {
    poem: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  };

  render() {
    const { poem } = this.props;
    if (!poem.poemName) return <p>Loading...</p>;
    return (
      <section
        id="text-3col-4"
        className="pt-50 pt-md-100 pb-md-50 pt-lg-150 pb-lg-100 light"
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-4">
              <h4>{this.props.t('poem.desc.subTitle')}</h4>
              <h2 className="mb-20">
                <strong>{poem.poemName}</strong>
              </h2>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height={20}
                viewBox="0 0 40 20"
                width={40}
                className="svg-secondary"
              >
                <path d="m0 8h40v4h-40z" fillRule="evenodd" />
              </svg>
              <p>{poem.poemDesc}</p>
            </div>
            <div className="col">{displayVerses(poem.verses)}</div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}

export default PoemDesc;
