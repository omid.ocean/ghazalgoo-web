import React from 'react';
import PropTypes from 'prop-types';

function SupportHeader(props) {
  return (
    <header
      id="header-text-halfbg"
      className="pt-50 pt-md-100 pb-md-100 pt-lg-200 pb-lg-200 dark"
    >
      <div className="container">
        <div className="row">
          <div className="col-md-8">
            <h2 className="text-secondary">{props.t('support.subTitle')}</h2>
            <h1 className="mb-50">
              <strong>{props.t('support.title')}</strong>
            </h1>
            <a href="#donate-desc-text">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height={100}
                viewBox="0 0 20 100"
                width="20px"
                className="svg-secondary"
              >
                <path
                  d="m11.5 91.5h4l-5.5 9-5.5-9h4v-91h3z"
                  fillRule="evenodd"
                />
              </svg>
            </a>
          </div>
          <div
            className="bg-box col-md-6 padding-x3 mt-50 mt-md-0"
            id="donate-header"
          />
        </div>
      </div>
      <div className="bg-wrap">
        <div className="bg" />
      </div>
    </header>
  );
}

SupportHeader.propTypes = {
  t: PropTypes.func.isRequired
};

export default SupportHeader;
