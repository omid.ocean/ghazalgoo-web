import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class SupportDesc extends Component {
  static propTypes = {
    t: PropTypes.func.isRequired
  };

  render() {
    return (
      <section
        id="subscribe-halfbg-counter"
        className="pt-0 pb-50 pt-md-100 pb-md-100 light"
      >
        <div className="container">
          <div className="row">
            <div
              className="bg-box col-md-6 mb-50 mb-md-0 embed-responsive-4by3"
              id="donate-desc-img"
            />
            <div className="col-md-6 col-lg-5 ml-auto" id="donate-desc-text">
              <h4 className="mb-30">
                <strong>{this.props.t('support.supportDesc.title')}</strong>
              </h4>
              <p className="mb-30">
                {this.props.t('support.supportDesc.desc')}
              </p>
              <form
                action="scripts/request.php"
                className="contact_form form-inline mb-20"
                id="subscribe-halfbg-counter-form"
                noValidate="novalidate"
              >
                <div className="form-group email-field-group">
                  <input
                    type="number"
                    className="form-control"
                    placeholder={this.props.t('support.supportDesc.amount')}
                    name="amount"
                  />
                </div>

                <button type="submit" className="btn btn-link">
                  <strong>{this.props.t('support.supportDesc.btn')}</strong>
                </button>
              </form>
              <p className="text-secondary small">
                {this.props.t('support.supportDesc.disclaimer')}
              </p>
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}
