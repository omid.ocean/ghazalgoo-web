import { faGoogle, faTelegram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Form } from 'informed';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { registerUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import { changePopupPage } from '../../actions/appStateActions';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {}
    };
  }
  setFormApi = formApi => {
    this.formApi = formApi;
  };

  handleSubmit = () => {
    this.props.registerUser(this.formApi.getState().values);
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errors: nextProps.error });
    }
  }

  //todo connect to i18l and translate
  render() {
    const { errors } = this.state;

    return (
      <div id="register">
        <div className="modal-body">
          <h3 className="mb-50">
            <strong>{this.props.t('auth.register.title')}</strong>
          </h3>
          <a className="btn btn-fb" href="#">
            <FontAwesomeIcon
              className="icon svg-default icon-pos-left dark"
              viewBox="0 0 400 400"
              icon={faTelegram}
            />
            {this.props.t('auth.register.withTelegram')}
          </a>
          <a className="btn btn-gp" href="#">
            <FontAwesomeIcon
              className="icon svg-default icon-pos-left dark"
              viewBox="0 0 400 450"
              icon={faGoogle}
            />
            {this.props.t('auth.register.withGoogle')}
          </a>
          <h4 className="mt-50">
            <strong>{this.props.t('auth.register.withEmail')}</strong>
          </h4>
          <Form
            className="contact_form form-vertical"
            noValidate
            getApi={this.setFormApi}
          >
            <TextFieldGroup
              type="text"
              placeHolder={this.props.t('auth.register.name')}
              field="name"
              error={errors.name}
            />
            <TextFieldGroup
              type="email"
              placeHolder={this.props.t('auth.register.email')}
              field="email"
              ltr
              error={errors.email}
            />
            <TextFieldGroup
              type="password"
              placeHolder={this.props.t('auth.register.password')}
              field="password"
              ltr
              error={errors.password}
            />
            <TextFieldGroup
              type="password"
              placeHolder={this.props.t('auth.register.password2')}
              field="password2"
              ltr
              error={errors.password2}
            />
            <p className="text txt-form small text-right">
              <span
                style={{ cursor: 'pointer' }}
                onClick={this.props.changePopupPage.bind(this, 'login')}
              >
                {this.props.t('auth.register.backToLogin')}
              </span>
            </p>
            <button
              type="submit"
              onClick={this.handleSubmit}
              className="btn btn-dark"
            >
              {this.props.t('auth.register.submitBtn')}
            </button>
          </Form>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error
});

export default compose(
  connect(
    mapStateToProps,
    { registerUser, changePopupPage }
  ),
  withNamespaces()
)(Register);
