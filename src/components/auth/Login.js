import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTelegram, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { Form } from 'informed';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { changePopupPage } from '../../actions/appStateActions';
import { loginUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {}
    };
  }

  setFormApi = formApi => {
    this.formApi = formApi;
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errors: nextProps.error });
    }
  }

  handleSubmit = () => {
    this.props.loginUser(this.formApi.getState().values);
  };

  //todo connect to i18l and translate
  render() {
    const { errors } = this.state;

    return (
      <div id="login">
        <div className="modal-body">
          <h3 className="mb-50">
            <strong>{this.props.t('auth.login.title')}</strong>
          </h3>
          <a className="btn btn-fb" href="#">
            <FontAwesomeIcon
              className="icon svg-default icon-pos-left dark"
              viewBox="0 0 400 400"
              icon={faTelegram}
            />
            {this.props.t('auth.login.telegram')}
          </a>
          <a className="btn btn-gp" href="#">
            <FontAwesomeIcon
              className="icon svg-default icon-pos-left dark"
              viewBox="0 0 400 450"
              icon={faGoogle}
            />
            {this.props.t('auth.login.google')}
          </a>
          <h4 className="mt-50">
            <strong>{this.props.t('auth.login.byEmail')}</strong>
          </h4>
          <Form
            className="contact_form form-vertical"
            noValidate
            getApi={this.setFormApi}
          >
            <TextFieldGroup
              type="email"
              placeHolder={this.props.t('auth.login.email')}
              field="email"
              error={errors.email}
            />
            <TextFieldGroup
              type="password"
              placeHolder={this.props.t('auth.login.password')}
              field="password"
              error={errors.password}
            />
            <p className="text txt-form small text-right">
              <span
                style={{ cursor: 'pointer' }}
                onClick={this.props.changePopupPage.bind(this, 'forgot')}
              >
                {this.props.t('auth.login.forgotPassword')}
              </span>
            </p>
            <div className="row">
              <button
                type="submit"
                onClick={this.handleSubmit}
                className="btn btn-dark"
              >
                {this.props.t('auth.login.loginBtn')}
              </button>
              <span
                style={{ cursor: 'pointer' }}
                onClick={this.props.changePopupPage.bind(this, 'register')}
                className="padding"
              >
                {this.props.t('auth.login.orRegister')}
              </span>
            </div>
          </Form>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  t: PropTypes.func.isRequired,
  changePopupPage: PropTypes.func.isRequired,
  loginUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error
});

export default compose(
  connect(
    mapStateToProps,
    { changePopupPage, loginUser }
  ),
  withNamespaces()
)(Login);
