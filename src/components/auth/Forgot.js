import React, { Component } from 'react';
import { Form, Text } from 'informed';
import TextFieldGroup from '../common/TextFieldGroup';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withNamespaces } from 'react-i18next';
import { changePopupPage } from '../../actions/appStateActions';
import PropTypes from 'prop-types';

class Forgot extends Component {
  constructor() {
    super();
    this.state = {
      errors: {}
    };
  }

  setFormApi = formApi => {
    this.formApi = formApi;
  };

  handleSubmit = () => {
    console.log(this.formApi.getState());
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errors: nextProps.error });
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div id="forgot">
        <div className="modal-body">
          <h3 className="mb-50">
            <strong>{this.props.t('auth.forgot.title')}</strong>
          </h3>
          <Form getApi={this.setFormApi} className="contact_form form-vertical">
            <TextFieldGroup
              type="email"
              placeHolder={this.props.t('auth.forgot.email')}
              field="email"
              error={errors.email}
            />
            <p className="text txt-form small text-right">
              <span
                style={{ cursor: 'pointer' }}
                onClick={this.props.changePopupPage.bind(this, 'login')}
              >
                {this.props.t('auth.forgot.backToLogin')}
              </span>
            </p>
            <button
              onClick={this.handleSubmit}
              type="submit"
              disabled
              className="btn btn-dark"
            >
              {this.props.t('auth.forgot.btnRestore')}
            </button>
          </Form>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </div>
    );
  }
}

Forgot.propTypes = {
  changePopupPage: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error
});

export default compose(
  connect(
    mapStateToProps,
    { changePopupPage }
  ),
  withNamespaces()
)(Forgot);
