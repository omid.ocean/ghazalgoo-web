import React from 'react';
import PropTypes from 'prop-types';
import profilePlaceHolder from '../../media/profile-placeHolder.jpg';
import { toPersianDate } from '../../utils/persianUtils';

const Comment = props => {
  const { comment } = props;
  return (
    <div className="media">
      <img
        className="mr-20 rounded-circle"
        src={comment.author.avatar ? comment.author.avatar : profilePlaceHolder}
        alt="profile"
        width="50px"
      />
      <div className="media-body">
        <p className="mb-0">
          <strong>
            <span>{comment.author.name}</span>
          </strong>
        </p>
        <p className="text-secondary small mb-10">
          {toPersianDate(comment.posted)} |
          <a href="#new-comment">
            <strong>{props.t('comments.comment.reply')}</strong>
          </a>
        </p>
        <div className="media-content">
          <p>{comment.text}</p>
        </div>
      </div>
    </div>
  );
};

Comment.propTypes = {
  t: PropTypes.func.isRequired,
  comment: PropTypes.object.isRequired
};

export default Comment;
