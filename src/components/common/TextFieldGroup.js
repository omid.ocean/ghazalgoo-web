import classNames from 'classnames';
import { asField } from 'informed';
import PropTypes from 'prop-types';
import React from 'react';

const TextFieldGroup = asField(({ fieldState, fieldApi, ...props }) => {
  const { value } = fieldState;
  const { setValue, setTouched } = fieldApi;
  const {
    placeHolder,
    error,
    type,
    disabled,
    ltr,
    validate,
    onChange,
    onBlur,
    forwardedRef
  } = props;
  return (
    <React.Fragment>
      <div className="form-group">
        <input
          ref={forwardedRef}
          value={!value && value !== 0 ? '' : value}
          onChange={e => {
            setValue(e.target.value);
            if (onChange) {
              onChange(e);
            }
          }}
          onBlur={e => {
            setTouched();
            if (onBlur) {
              onBlur(e);
            }
          }}
          type={type}
          className={classNames('form-control', {
            'invalid-field': !!error
          })}
          placeholder={placeHolder}
          disabled={disabled}
          validate={validate}
          dir={ltr ? 'ltr' : 'rtl'}
        />
        {!!error && <div class="invalid-feedback">{error}</div>}
      </div>
    </React.Fragment>
  );
});

TextFieldGroup.prototypes = {
  name: PropTypes.string.isRequired,
  placeHolder: PropTypes.string.isRequired,
  error: PropTypes.string,
  type: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  ltr: PropTypes.bool
};

export default TextFieldGroup;
