import classNames from 'classnames';
import { asField } from 'informed';
import PropTypes from 'prop-types';
import React from 'react';

const TextAreaGroup = asField(({ fieldState, fieldApi, ...props }) => {
  const { value } = fieldState;
  const { setValue, setTouched } = fieldApi;
  const {
    placeHolder,
    error,
    disabled,
    ltr,
    validate,
    onChange,
    onBlur,
    forwardedRef,
    rows
  } = props;
  return (
    <React.Fragment>
      <div className="form-group textarea-group">
        <textarea
          ref={forwardedRef}
          value={!value && value !== 0 ? '' : value}
          onChange={e => {
            setValue(e.target.value);
            if (onChange) {
              onChange(e);
            }
          }}
          onBlur={e => {
            setTouched();
            if (onBlur) {
              onBlur(e);
            }
          }}
          className={classNames('form-control', {
            'invalid-field': !!error
          })}
          placeholder={placeHolder}
          disabled={disabled}
          validate={validate}
          dir={ltr ? 'ltr' : 'rtl'}
          rows={rows}
        />
        {!!error && <div class="invalid-feedback">{error}</div>}
      </div>
    </React.Fragment>
  );
});

TextAreaGroup.prototypes = {
  name: PropTypes.string.isRequired,
  placeHolder: PropTypes.string.isRequired,
  error: PropTypes.string,
  disabled: PropTypes.bool,
  ltr: PropTypes.bool,
  rows: PropTypes.number
};

export default TextAreaGroup;
