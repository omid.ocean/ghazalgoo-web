import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Comment from './Comment';

export default class Comments extends Component {
  static propTypes = {
    t: PropTypes.func.isRequired,
    comments: PropTypes.array.isRequired
  };

  render() {
    const { comments } = this.props;
    return (
      <section id="inner-comments" className="pt-50 pb-50 light">
        <div className="container">
          <div className="row">
            <div className="col-md-12 ml-auto mr-auto">
              <h3>
                {this.props.t('comments.title', {
                  numberOfComments: comments.length
                })}
              </h3>
              {comments.length > 0 &&
                comments.map(el => <Comment comment={el} t={this.props.t} />)}
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}
