import React from 'react';
import '../../styles/pre-loader.css';

export default function PreLoader() {
  return (
    <section className="preLoaderWrapper">
      <div className="spinner centerScreen">
        <i />
        <i />
        <i />
        <i />
        <i />
        <i />
        <i />
      </div>
    </section>
  );
}
