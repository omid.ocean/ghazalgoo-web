import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'informed';
import { sendPoemComment } from '../../actions/poemActions';
import TextFieldGroup from './TextFieldGroup';
import TextAreaGroup from './TextAreaGroup';
import { connect } from 'react-redux';

class NewComment extends Component {
  static propTypes = {
    t: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      errors: {}
    };
  }

  setFormApi = formApi => {
    this.formApi = formApi;
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors && nextProps.errors.data) {
      this.setState({ errors: nextProps.errors.data });
    } else {
      this.setState({ errors: {} });
    }
  }

  handleSubmit = () => {
    console.log(this.formApi.getState().values, this.props.poemID);
    this.props.sendPoemComment(
      this.formApi.getState().values,
      this.props.poemID
    );
  };

  render() {
    let { errors } = this.state;
    return (
      <section id="new-comment" className="pt-75 pb-75 light">
        <div className="container">
          <div className="row">
            <div className="col-md-10 ml-auto mr-auto">
              <div className="content-box padding-x3 border">
                <h3 className="mb-30">
                  {this.props.t('comments.newComment.title')}
                </h3>
                <Form
                  className="contact_form form-inline"
                  id="contact-form-3-form"
                  noValidate
                  getApi={this.setFormApi}
                >
                  {!this.props.auth.isAuthenticated && (
                    <TextFieldGroup
                      type="text"
                      placeHolder={this.props.t('comments.newComment.name')}
                      field="name"
                      error={errors.name}
                      disabled={this.props.auth.isAuthenticated}
                    />
                  )}
                  {!this.props.auth.isAuthenticated && (
                    <TextFieldGroup
                      type="email"
                      placeHolder={this.props.t('comments.newComment.email')}
                      field="email"
                      error={errors.email}
                    />
                  )}
                  <TextAreaGroup
                    placeHolder={this.props.t(
                      'comments.newComment.yourComment'
                    )}
                    field="text"
                    error={errors.text}
                    rows={5}
                  />
                  <button
                    type="submit"
                    onClick={this.handleSubmit}
                    className="btn btn-dark mt-30 btn-block"
                  >
                    {this.props.t('comments.newComment.send')}
                  </button>
                </Form>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  poemID: state.poem.poemFull._id,
  errors: state.error
});

export default connect(
  mapStateToProps,
  { sendPoemComment }
)(NewComment);
