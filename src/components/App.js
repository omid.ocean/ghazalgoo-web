import i18n from 'i18next';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { changeLanguage } from '../actions/appStateActions';
import { store } from '../store/store';
import '../styles/bootstrap.weber.css';
import '../styles/custom.css';
import '../styles/fonts.css';
import '../styles/fx.css';
import '../styles/magnific-popup.css';
import '../styles/owl.carousel.css';
import '../styles/rtl.css';
import { loadToken } from '../utils/tokenLoader';
import Footer from './main/Footer';
import Home from './main/Home';
import Navigation from './main/Navigation';
import Poem from './main/Poem';
import Poet from './main/Poet';
import PopUp from './main/PopUp';
import Contact from './main/Contact';
import Support from './main/Support';
import About from './main/About';

loadToken(store);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: 'fa'
    };
  }

  /**
   * changes language by language code. if not provided will just change it
   * @param {string | undefined} langCode
   */
  changeLanguage = langCode => {
    const { language } = i18n;
    if (langCode === undefined && language === 'fa') {
      langCode = 'en';
    } else if (langCode === undefined && language === 'en') {
      langCode = 'fa';
    }
    store.dispatch(changeLanguage(langCode));
    this.setState({ language: langCode });
  };

  /**
   * @todo @see https://maisano.github.io/react-router-transition/animated-switch/code animate Switch
   */
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className={`wrap ${i18n.dir(this.state.language)}`}>
            <Navigation changeLanguage={this.changeLanguage} />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/poet/:id" component={Poet} />
              <Route exact path="/poem/:id" component={Poem} />
              <Route exact path="/contact-us" component={Contact} />
              <Route exact path="/donate" component={Support} />
              <Route exact path="/about-us" component={About} />
            </Switch>
            <Footer />
            <PopUp />
            <ToastContainer autoClose={8000} rtl position="bottom-left" />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
