import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCategoryByID } from '../../actions/categoryActions';
import { Link } from 'react-router-dom';
import Loader from 'react-loader-spinner';

class PoetCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      catID: null,
      parent: null,
      isLoading: true
    };
  }

  static propTypes = {
    getCategoryByID: PropTypes.func.isRequired,
    poetMainCatID: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired
  };

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.poetMainCatID &&
      nextProps.poetMainCatID !== this.props.poetMainCatID
    ) {
      this.setState({
        catID: nextProps.poetMainCatID,
        isLoading: false
      });
    }

    if (
      nextProps.currentTopCat &&
      nextProps.currentTopCat !== this.state.catID
    ) {
      this.setState({
        parent: nextProps.poetMainCatID,
        isLoading: false
      });
    }
  }

  componentDidMount() {
    this.props.getCategoryByID(this.props.poetMainCatID);
  }

  changeTopCat = id => {
    if (this.props.category.isParent) {
      this.setState({ parent: this.props.category._id, isLoading: true });
    } else {
      this.setState({ isLoading: true });
    }
    this.props.getCategoryByID(id);
  };

  renderSubCategories = subCategories => {
    return subCategories.map(el => {
      return (
        <div className="col-sm-6" key={el._id}>
          <div className="d-flex mb-50">
            <div>
              <span
                className="sub-category"
                onClick={this.changeTopCat.bind(this, el._id)}
              >
                {el.title}
              </span>
            </div>
          </div>
        </div>
      );
    });
  };

  renderPoems = poems => {
    return poems.map(el => {
      return (
        <div className="col-sm-6" key={el._id}>
          <div className="d-flex mb-50">
            <Link to={`/poem/${el._id}`} className="sub-poem">
              {el.poemName}
            </Link>
          </div>
        </div>
      );
    });
  };

  static propTypes = {
    category: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  };

  render() {
    const { category } = this.props;
    if (!category.title) return <p>Loading</p>;
    return (
      <section
        id="poems"
        className="pt-100 pb-50 counter-up light spr-oc-show spr-wout"
      >
        {this.state.isLoading && (
          <div id="cat-loading">
            <div className="loader">
              <Loader type="ThreeDots" color="#333" height={80} width={80} />
            </div>
          </div>
        )}

        <div id="cat-data" className="container">
          {category.title && (
            <div className="row">
              <div className="col-lg-3 mr-auto">
                <h3>
                  <strong>{category.title}</strong>
                </h3>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  height={20}
                  viewBox="0 0 40 20"
                  width={40}
                  className="mb-30 svg-primary"
                  alt="sep"
                >
                  <path d="m0 8h40v4h-40z" fillRule="evenodd" />
                </svg>
                <p className="mb-50">
                  {!category.isParent && (
                    <span
                      onClick={this.changeTopCat.bind(this, this.state.parent)}
                      style={{ cursor: 'pointer' }}
                    >
                      {this.props.t('poet.poetCategories.backToMain')}
                    </span>
                  )}
                  {category.desc}
                </p>
              </div>
              <div className="col-lg-8">
                <div className="row">
                  {this.renderSubCategories(category.subCategories)}
                </div>
                {category.poems && (
                  <div className="poems">
                    <div className="row separator">
                      <div className="col-lg-3">
                        <h3>
                          <strong>
                            {this.props.t('poet.poetCategories.poems')}
                          </strong>
                        </h3>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          height={20}
                          viewBox="0 0 40 20"
                          width={40}
                          className="mb-30 svg-primary"
                          src="images/gallery/decor/line-h-1.svg"
                          alt="sep"
                        >
                          <path d="m0 8h40v4h-40z" fillRule="evenodd" />
                        </svg>
                      </div>
                    </div>
                    <div className="row separator">
                      {this.renderPoems(category.poems)}
                    </div>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  category: state.category.categoryFull,
  poetMainCatID: state.poet.poetFull.category,
  currentTopCat: state.category.currentTopCat
});

export default connect(
  mapStateToProps,
  { getCategoryByID }
)(PoetCategories);
