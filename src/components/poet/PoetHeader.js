import React, { PureComponent } from 'react';
import OwlCarousel from 'react-owl-carousel2';
import PropTypes from 'prop-types';
import logo from '../../media/logo-4.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class PoetHeader extends PureComponent {
  options = {
    loop: false,
    margin: 0,
    nav: true,
    autoplay: true,
    autoplayHoverPause: true,
    autoHeight: false,
    items: 1,
    dots: true,
    navText: ['', ''],
    rewind: true
  };

  static propTypes = {
    poet: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  };

  addtoFavs = () => {
    //todo add poet to favs
  };
  render() {
    const { poet } = this.props;

    return (
      <header
        id="header-carousel-2"
        className="section-carousel overall text-center light carousel-dots-center-bottom carousel-nav-aside-center"
      >
        <OwlCarousel
          className="carousel-single carousel-stretch owl-drag"
          ref="poetDetails"
          options={this.options}
        >
          <div className="item w-100 d-flex align-items-center pt-75 pb-75 pt-md-150 pb-md-150">
            <div className="container">
              <div className="row">
                <div className="col-md-8 ml-auto mr-auto">
                  <div className="content-box padding-x3 bg-default shadow">
                    <div className="position-absolute t-0 l-50 centered">
                      <img alt="logo" src={logo} height="120px" />
                    </div>
                    <h2 className="mt-50">
                      <strong>{poet.name}</strong>
                    </h2>
                    <h3 className="text-secondary mb-50">
                      {this.props.t('poet.poetHeader.birth')} : ... <br />{' '}
                      {this.props.t('poet.poetHeader.death')} : ...
                    </h3>
                    <a href="#poems" className="btn btn-lg btn-dark">
                      {this.props.t('poet.poetHeader.poems')}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="item w-100 d-flex align-items-center pt-75 pb-75 pt-md-150 pb-md-150">
            <div className="container">
              <div className="row">
                <div className="col-md-8 ml-auto mr-auto">
                  <div className="content-box padding-x3 bg-default shadow">
                    <FontAwesomeIcon
                      className="mb-50 icon svg-default"
                      viewBox="0 0 100 100"
                      icon="file-alt"
                    />
                    <h3 className="mb-30">
                      <strong>{this.props.t('poet.poetHeader.about')}</strong>
                    </h3>
                    <p className="mb-50">{poet.description}</p>
                    <a href="#poems" className="btn btn-primary">
                      {this.props.t('poet.poetHeader.wikipediaPage')}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="item w-100 d-flex align-items-center pt-75 pb-75 pt-md-150 pb-md-150">
            <div className="container">
              <div className="row">
                <div className="col-md-8 ml-auto mr-auto">
                  <div className="content-box padding-x3 bg-default shadow">
                    <FontAwesomeIcon
                      className="mb-50 icon svg-default"
                      viewBox="0 0 150 150"
                      icon="thumbs-up"
                    />
                    <h3 className="mb-30">
                      <strong>
                        {this.props.t('poet.poetHeader.Popularity')}
                      </strong>
                    </h3>
                    <p className="mb-50">
                      {this.props.t('poet.poetHeader.PopularityDesc', {
                        likes: poet.likes
                      })}
                    </p>
                    <span onClick={this.addtoFavs} className="btn btn-primary">
                      {this.props.t('poet.poetHeader.addToFavs')}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </OwlCarousel>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </header>
    );
  }
}
