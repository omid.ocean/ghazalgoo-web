import React, { Component } from 'react';
import HomeHeader from '../home/HomeHeader';
import '../../styles/index.css';
import HomeIntro from '../home/HomeIntro';
import HomeCounter from '../home/HomeCounter';
import HomeRandomPoem from '../home/HomeRandomPoem';
import HomeArtists from '../home/HomeArtists';
import { withNamespaces } from 'react-i18next';

class Home extends Component {
  render() {
    return (
      <div>
        <HomeHeader t={this.props.t} light={false} />
        <HomeIntro t={this.props.t} light={false} />
        <HomeCounter
          t={this.props.t}
          light={false}
          poemCount={63000}
          poetCount={72}
          verseCount={1490000}
        />
        <HomeRandomPoem light={false} stars={5} />
        <HomeArtists t={this.props.t} light={true} />
      </div>
    );
  }
}

export default withNamespaces()(Home);
