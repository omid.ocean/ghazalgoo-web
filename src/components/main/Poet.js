import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { getPoetByID } from '../../actions/poetActions';
import '../../styles/author-details.css';
import PreLoader from '../common/PreLoader';
import PoetCategories from '../poet/PoetCategories';
import PoetHeader from '../poet/PoetHeader';
class Poet extends PureComponent {
  static propTypes = {
    poet: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired
  };

  componentWillMount() {
    if (this.props.poet._id !== this.props.match.params.id)
      this.props.getPoetByID(this.props.match.params.id);
  }

  render() {
    const { poet } = this.props;

    if (!poet.name || poet._id !== this.props.match.params.id) {
      return <PreLoader />;
    }

    return (
      <div>
        <PoetHeader t={this.props.t} poet={poet} />
        <PoetCategories t={this.props.t} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  poet: state.poet.poetFull
});

export default compose(
  withNamespaces(),
  connect(
    mapStateToProps,
    { getPoetByID }
  )
)(Poet);
