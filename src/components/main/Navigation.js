import React, { Component } from 'react';
import logo from '../../media/logo-2-light.png';
import { withNamespaces } from 'react-i18next';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { changePopupPage } from '../../actions/appStateActions';
import { logoutUser } from '../../actions/authActions';
import PropTypes from 'prop-types';

//todo add specific languages buttons
class Navigation extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    changePopupPage: PropTypes.func.isRequired,
    logoutUser: PropTypes.func.isRequired,
    changeLanguage: PropTypes.func.isRequired
  };

  renderPopupButtons = () => {
    if (this.props.isAuthenticated) {
      return (
        <ul className="navbar-nav">
          <li className="nav-item">
            <span
              className="nav-link"
              style={{ cursor: 'pointer' }}
              onClick={this.props.changePopupPage.bind(this, 'profile')}
            >
              {this.props.t('navigation.profile')}
            </span>
          </li>
          <li className="nav-item">
            <span
              className="nav-link"
              style={{ cursor: 'pointer' }}
              onClick={this.props.logoutUser}
            >
              {this.props.t('navigation.logout')}
            </span>
          </li>
        </ul>
      );
    } else {
      return (
        <ul className="navbar-nav">
          <li className="nav-item">
            <span
              className="nav-link"
              style={{ cursor: 'pointer' }}
              onClick={this.props.changePopupPage.bind(this, 'register')}
            >
              {this.props.t('navigation.register')}
            </span>
          </li>
          <li className="nav-item">
            <span
              className="nav-link"
              style={{ cursor: 'pointer' }}
              onClick={this.props.changePopupPage.bind(this, 'login')}
            >
              {this.props.t('navigation.login')}
            </span>
          </li>
        </ul>
      );
    }
  };

  render() {
    return (
      <nav id="nav-megamenu-logo-menu" className="navbar dark navbar-expand-lg">
        <div className="container-fluid">
          <div className="row align-items-center">
            <div className="col-4 hidden-lg">
              <button
                className="navbar-toggler collapsed"
                type="button"
                data-toggle="collapse"
                data-target=".main-menu-collapse"
                aria-controls="navbarMenuContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
            </div>
            <div className="col-4 col-lg-2 order-lg-2 text-center">
              <span
                className="navbar-brand"
                onClick={this.props.changeLanguage.bind(undefined, undefined)}
              >
                <img src={logo} height={30} alt="" className="mw-100" />
              </span>
            </div>
            <div className="col-lg-5 order-lg-1 collapse navbar-collapse main-menu-collapse position-inherit">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink
                    exact
                    className="nav-link"
                    activeClassName="active-page"
                    to="/about-us"
                  >
                    {this.props.t('navigation.aboutUs')}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    exact
                    activeClassName="active-page"
                    to="/contact-us"
                  >
                    {this.props.t('navigation.contactUs')}
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    exact
                    activeClassName="active-page"
                    to="/donate"
                  >
                    {this.props.t('navigation.donate')}
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="col-lg-5 order-lg-3 collapse navbar-collapse main-menu-collapse justify-content-lg-end">
              {this.renderPopupButtons()}
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default compose(
  withNamespaces(),
  connect(
    mapStateToProps,
    { changePopupPage, logoutUser }
  )
)(Navigation);
