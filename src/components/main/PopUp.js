import React, { Component } from 'react';
import Popup from 'reactjs-popup';
import Login from '../auth/Login';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Register from '../auth/Register';
import Forgot from '../auth/Forgot';
import { connect } from 'react-redux';
import { closePopUp } from '../../actions/appStateActions';

class PopUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      popupPage: '',
      popup: false
    };
  }

  componentWillMount() {
    if (
      this.props.popup !== undefined &&
      this.props.popup !== this.state.popup
    ) {
      this.setState({ popup: this.props.popup });
    }
    if (this.props.popupPage && this.props.popupPage !== this.state.popupPage) {
      this.setState({ popupPage: this.props.popupPage });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.popup !== undefined && nextProps.popup !== this.state.popup) {
      this.setState({ popup: nextProps.popup });
    }
    if (nextProps.popupPage && nextProps.popupPage !== this.state.popupPage) {
      this.setState({ popupPage: nextProps.popupPage });
    }
  }

  popupPage = () => {
    if (this.props.isAuthenticated) {
      return <p>Profile</p>;
    }

    switch (this.state.popupPage) {
      case 'login':
        return <Login />;
      case 'register':
        return <Register />;
      case 'forgot':
        return <Forgot />;
      case 'profile':
        return <p>PROFILE PAGE</p>;
      default:
        return <Login />;
    }
  };

  render() {
    return (
      <Popup
        className="modal-container"
        lockScroll={true}
        open={this.state.popup}
        onClose={this.props.closePopUp}
        contentStyle={{ width: 'auto' }}
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content padding-x3 light">
            <button
              type="button"
              className="close position-absolute r-0 t-0"
              onClick={this.props.closePopUp}
            >
              <FontAwesomeIcon
                className="icon svg-secondary"
                viewBox="0 0 350 350"
                icon="times"
              />
            </button>
            {this.popupPage()}
          </div>
        </div>
      </Popup>
    );
  }
}

const mapStateToProps = state => ({
  popup: state.appState.popup,
  popupPage: state.appState.popupPage,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps,
  { closePopUp }
)(PopUp);
