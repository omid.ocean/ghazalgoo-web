import React from 'react';
import googlePlay from '../../media/btn-google.png';
import googlePlayRetina from '../../media/btn-google@2x.png';
import appStore from '../../media/btn-apple.png';
import appStoreRetina from '../../media/btn-apple@2x.png';
import logo from '../../media/logo-4-light.png';
import { withNamespaces } from 'react-i18next';
import { Link } from 'react-router-dom';

const Footer = props => {
  return (
    <footer id="footer-logo-text-img" className="pt-30 pb-30 text-center dark">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-md order-md-2">
            <ul className="list-unstyled list-justify padding-list mb-20 mb-md-0">
              <li>
                <Link to="/about-us">
                  <strong>{props.t('footer.aboutUs')}</strong>
                </Link>
              </li>
              <li>
                <Link to="/contact-us">
                  <strong>{props.t('footer.contactUs')}</strong>
                </Link>
              </li>
              <li>
                <Link to="/donate">
                  <strong>{props.t('footer.donate')}</strong>
                </Link>
              </li>
            </ul>
          </div>
          <div className="col-md-auto order-md-3">
            <a href="http://apple.com" style={{ padding: '0 10px' }}>
              <img
                src={appStore}
                srcSet={`${appStoreRetina} 2x`}
                alt="App store"
              />
            </a>
            <a href="http://google.com">
              <img
                src={googlePlay}
                srcSet={`${googlePlayRetina} 2x`}
                alt="Google play"
              />
            </a>
          </div>
          <div className="col-md-auto order-md-1">
            <Link to="/">
              <img
                className="mw-100 mt-30 mt-md-0"
                src={logo}
                height="60px"
                alt="logo"
              />
            </Link>
          </div>
        </div>
      </div>
      <div className="bg-wrap">
        <div className="bg" />
      </div>
    </footer>
  );
};

export default withNamespaces()(Footer);
