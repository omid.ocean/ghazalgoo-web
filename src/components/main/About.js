import React from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import descIMG from '../../media/about-desc.jpg';

function About(props) {
  return (
    <section
      id="benefits-2col-10"
      className="pt-50 pt-md-100 pb-md-50 text-center light spr-oc-show spr-wout"
    >
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h3>
              <strong>{props.t('aboutUS.title')}</strong>
            </h3>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height={20}
              viewBox="0 0 40 20"
              width={40}
              className="svg-secondary mb-20"
              alt="sep"
            >
              <path d="m0 8h40v4h-40z" fillRule="evenodd" />
            </svg>
            <p className="text-secondary mb-50">{props.t('aboutUS.desc')}</p>
            <div className="w-100" />
            <div className="content-box d-inline-block">
              <img
                alt="img"
                className="mw-100 mb-50 shadow-alt"
                src={descIMG}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="bg-wrap">
        <div className="bg" />
      </div>
    </section>
  );
}

About.propTypes = {
  t: PropTypes.func.isRequired
};

export default withNamespaces()(About);
