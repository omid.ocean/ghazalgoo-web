import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import '../../styles/contact.css';

class Contact extends Component {
  static propTypes = {
    t: PropTypes.func.isRequired
  };

  render() {
    return (
      <section id="contact-text-form" className="pt-150 pb-150 dark">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-5 mr-auto">
              <h3>
                <strong>{this.props.t('contact.title')}</strong>
              </h3>
              <p className="mb-50 text-secondary">
                {this.props.t('contact.subTitle')}
              </p>
            </div>
            <div className="col-lg-6">
              <div className="content-box padding-x3 bg-default light">
                <h3>{this.props.t('contact.FormTitle')}</h3>
                <form
                  action="./scripts/request.php"
                  className="contact_form mb-30 form-vertical"
                  id="contact-text-form-form"
                  noValidate="novalidate"
                >
                  <div className="form-group text-field-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder={this.props.t('contact.name')}
                      name="NAME"
                      size={10}
                    />
                  </div>
                  <div className="form-group email-field-group">
                    <input
                      type="email"
                      className="form-control"
                      placeholder={this.props.t('contact.email')}
                      name="EMAIL"
                      size={10}
                    />
                  </div>
                  <div className="form-group textarea-group">
                    <textarea
                      className="form-control"
                      placeholder={this.props.t('contact.message')}
                      rows={5}
                      name="MESSAGE"
                      defaultValue={''}
                    />
                  </div>
                  <button
                    type="submit"
                    className="btn btn-block btn-dark mt-30"
                  >
                    {this.props.t('contact.sendMessage')}
                  </button>
                </form>
                <p className="small text-secondary mb-0">
                  {this.props.t('contact.confidencialText')}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-wrap">
          <div className="bg" />
        </div>
      </section>
    );
  }
}

export default withNamespaces()(Contact);
