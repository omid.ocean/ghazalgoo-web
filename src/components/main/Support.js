import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SupportHeader from '../support/SupportHeader';
import { withNamespaces } from 'react-i18next';
import '../../styles/donate.css';
import SupportDesc from '../support/SupportDesc';
class Support extends Component {
  static propTypes = {
    prop: PropTypes
  };

  render() {
    return (
      <div>
        <SupportHeader t={this.props.t} />
        <SupportDesc t={this.props.t} />
      </div>
    );
  }
}

export default withNamespaces()(Support);
