import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { withNamespaces } from 'react-i18next';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { getPoemByID } from '../../actions/poemActions';
import '../../styles/poem.css';
import PreLoader from '../common/PreLoader';
import PoemDesc from '../poem/PoemDesc';
import PoemHeader from '../poem/PoemHeader';
import Comments from '../common/Comments';
import NewComment from '../common/NewComment';

class Poem extends PureComponent {
  static propTypes = {
    poem: PropTypes.object.isRequired,
    t: PropTypes.object.isRequired,
    getPoemByID: PropTypes.func.isRequired
  };

  componentDidMount() {
    if (this.props.match.params.id.length > 4)
      this.props.getPoemByID(this.props.match.params.id);
  }

  componentWillUnmount() {
    this.props.getPoemByID(undefined);
  }

  render() {
    const { poem } = this.props;

    if (!poem.poemName) {
      return <PreLoader />;
    }

    return (
      <div>
        <PoemHeader poemName={poem.poemName} poet={poem.poet} />
        <PoemDesc poem={poem} t={this.props.t} />
        <Comments t={this.props.t} comments={poem.comments} />
        <NewComment t={this.props.t} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  poem: state.poem.poemFull
});

export default compose(
  withNamespaces(),
  connect(
    mapStateToProps,
    { getPoemByID }
  )
)(Poem);
