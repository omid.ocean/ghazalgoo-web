import axios from 'axios';

/**
 * sets axios Authorization header if token is a string. deletes it if it's undefined
 * @param {string | undefined} token
 * @see axios
 */
const setAuthToken = token => {
  if (token) {
    // Apply to every request
    axios.defaults.headers.common['Authorization'] = token;
  } else {
    // Delete auth Header
    delete axios.defaults.headers.common['Authorization'];
  }
};

export { setAuthToken };
