import { sortBy } from 'lodash';
import React from 'react';

/**
 * Peocesses the verses recieved from database. orders them and processes them to pairs with regard to their position(isFirst) and their order;
 * @function
 * @param {Array} verses - ordered or un ordered verses
 * @param {number} maxVerses - max number of displayed verses
 * @returns {Array} - returns ordered array with regard to verse position (is first or not)
 */
const processedVerse = (verses, maxVerses) => {
  const pairs = [];
  const unProcessArr = sortBy(verses, ['order', 'isFirst']);
  let skip = -1;
  for (var i = 0; i < unProcessArr.length; i += 2) {
    if (maxVerses !== undefined && maxVerses === i) break;
    if (i === skip) continue;
    if (unProcessArr[i + 1] !== undefined && !unProcessArr[i + 1].isFirst) {
      pairs.push([unProcessArr[i], unProcessArr[i + 1]]);
    } else if (
      unProcessArr[i + 1] !== undefined &&
      unProcessArr[i + 1].isFirst
    ) {
      pairs.push([unProcessArr[i]]);
      i--;
    } else if (
      unProcessArr[i + 1] !== undefined &&
      unProcessArr[i + 1].isFirst &&
      unProcessArr[i + 2] !== undefined &&
      !unProcessArr[i + 2].isFirst
    ) {
      pairs.push([unProcessArr[i + 1], unProcessArr[i + 2]]);
      skip = i + 2;
    } else {
      pairs.push([unProcessArr[i]]);
    }
  }
  return pairs;
};

/**
 * processes the paired verses and returns the jsx representation of 'Beit'.
 * @function
 * @param {Array} verses - processed, ordered, paired verses. from @function processedVerse
 * @param {number=} maxVerses - max number of displayed verses
 * @param {boolean=} limitLine - limit each line to 150 characters
 * @returns {JSX} - returns JSX representation of verses.
 */

const displayVerses = (verses, maxVerses, limitLine) => {
  const processed = processedVerse(verses, maxVerses);
  return processed.map(val => {
    if (val.length === 2) {
      return (
        <div className="row" key={val[0]._id}>
          <div className="col-md verse">
            {limitLine ? val[0].text.substring(0, 150) : val[0].text}
          </div>
          <div className="col-md verse">
            {limitLine ? val[1].text.substring(0, 150) : val[1].text}
          </div>
        </div>
      );
    } else {
      return (
        <div className="row" key={val[0]._id}>
          <div className="col-md verse">
            {limitLine ? val[0].text.substring(0, 150) : val[0].text}
          </div>
        </div>
      );
    }
  });
};

export { processedVerse, displayVerses };
