import { setAuthToken } from '../utils/networkingTools';
import { setCurrentUser, logoutUser } from '../actions/authActions';
import jwt_decode from 'jwt-decode';

/**
 * loads jwt token from local storage. and logsout and removes it if it's expired
 * @param {function} store
 */
const loadToken = store => {
  if (localStorage.jwtToken) {
    // Set auth token header auth
    setAuthToken(localStorage.jwtToken);
    // Decode token and get user info and exp
    const decoded = jwt_decode(localStorage.jwtToken);
    // Set user and is Authenticated
    store.dispatch(setCurrentUser(decoded));

    // check for expired token
    const curentTime = Date.now() / 1000;
    if (decoded.exp < curentTime) {
      // Logout user
      store.dispatch(logoutUser);
      // TODO: Clear current profile
    }
  }
};

export { loadToken };
