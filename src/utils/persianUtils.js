import PersianDate from 'persian-date';

const toPersianDate = date => {
  const pDate = new PersianDate(new Date(date));
  return pDate.format('D MMMM YY - H:m');
};

export { toPersianDate };
