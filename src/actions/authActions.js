import { GET_ERRORS, REGISTER_USER, SET_CURRENT_USER } from './types';
import Axios from 'axios';
import { toast } from 'react-toastify';
import { t as translate } from 'i18next';
import { setAuthToken } from '../utils/networkingTools';
import jwt_decode from 'jwt-decode';

const registerUser = userData => dispatch => {
  Axios.post('/api/users/register', userData)
    .then(res => {
      toast.success(translate('toast.registerSuccess'));
      dispatch({
        type: REGISTER_USER,
        payload: res.data
      });
    })
    .catch(err => {
      toast.error(translate('toast.registerFail'));
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

const loginUser = userData => dispatch => {
  Axios.post('/api/users/login', userData)
    .then(res => {
      // Save to local Storage
      const { token } = res.data;
      // Set token to local storage
      localStorage.setItem('jwtToken', token);
      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user Data
      const decoded = jwt_decode(token);
      // login success notif
      toast.success(translate('toast.loginSuccess'));
      // Set current User
      dispatch(setCurrentUser(decoded));
    })
    .catch(err => {
      if (err.response.data.combination)
        toast.error(err.response.data.combination);
      else toast.error(translate('toast.loginFail'));
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};
// Set logged in user
const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

const logoutUser = () => dispatch => {
  //remove token from local storage
  localStorage.removeItem('jwtToken');
  // Remove Auth header for future requests
  setAuthToken(false);
  // Send Toast
  toast.success(translate('toast.logoutSuccess'));
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};

export { registerUser, loginUser, setCurrentUser, logoutUser };
