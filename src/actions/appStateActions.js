import { CHANGE_PAGE, CLOSE_POPUP, OPEN_POPUP, CHANGE_LANGUAGE } from './types';
import { i18n } from '../startup/i18n';

/**
 * redux action from closing popup
 */
const closePopUp = () => {
  return {
    type: CLOSE_POPUP
  };
};

/**
 * redux action for opening popup
 */
const openPopup = () => {
  return {
    type: OPEN_POPUP
  };
};

/**
 * change popup page
 * @param {string} pageName pagename
 * @todo convert param to enum
 */
const changePopupPage = pageName => {
  return {
    type: CHANGE_PAGE,
    payload: pageName
  };
};

/**
 * changed app language
 * @see i18n
 * @param {string} langCode two letter language code
 * @todo use enum for supported languages. fall back to persian if not found
 */
const changeLanguage = langCode => {
  i18n.changeLanguage(langCode);
  return {
    type: CHANGE_LANGUAGE,
    payload: langCode
  };
};

export { closePopUp, openPopup, changePopupPage, changeLanguage };
