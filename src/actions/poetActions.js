import Axios from 'axios';
import { GET_POET, GET_ERRORS, GET_POETS_LIST, REMOVE_POET } from './types';

const getPoetByID = poetID => dispatch => {
  if (poetID === undefined) {
    dispatch({ type: REMOVE_POET });
  }

  Axios.get(`/api/poets/${poetID}`)
    .then(res =>
      dispatch({
        type: GET_POET,
        payload: res.data.poet
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response
      })
    );
};

const getPoetList = page => dispatch => {
  page = page && page > 0 ? page : 1;
  Axios.get(`/api/poets/?page=${page}`)
    .then(res =>
      dispatch({
        type: GET_POETS_LIST,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response
      })
    );
};

export { getPoetByID, getPoetList };
