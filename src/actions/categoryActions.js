import Axios from 'axios';
import { GET_CATEGORY, GET_ERRORS, SET_CURRENT_TOP_CAT } from './types';

const getCategoryByID = catId => dispatch => {
  Axios.get(`/api/categories/${catId}`)
    .then(res =>
      dispatch({
        type: GET_CATEGORY,
        payload: res.data.category
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

const changeCurrentTopCat = catID => {
  return {
    type: SET_CURRENT_TOP_CAT,
    payload: catID
  };
};
export { getCategoryByID, changeCurrentTopCat };
