import Axios from 'axios';
import {
  GET_POEM,
  GET_RANDOM_POEM,
  GET_ERRORS,
  REMOVE_POEM,
  SEARCH_POEM,
  COMMENT_ADDED
} from './types';

const getPoemByID = poemID => dispatch => {
  if (poemID === undefined) dispatch({ type: REMOVE_POEM });
  Axios.get(`/api/poems/id/${poemID}`)
    .then(res =>
      dispatch({
        type: GET_POEM,
        payload: res.data.poem
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response
      })
    );
};

const getRandomPoem = () => dispatch => {
  Axios.get(`/api/poems/random`)
    .then(res =>
      dispatch({
        type: GET_RANDOM_POEM,
        payload: res.data.poem[0]
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response
      })
    );
};

const queryPoems = (query, page) => dispatch => {
  Axios.get(`/api/poems/?search="${encodeURIComponent(query)}"&page=${page}`)
    .then(res =>
      dispatch({
        type: SEARCH_POEM,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response
      })
    );
};

const sendPoemComment = (data, poemID) => dispatch => {
  Axios.post(`/api/poems/comment/${poemID}`, data)
    .then(res =>
      dispatch({
        type: COMMENT_ADDED,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response
      })
    );
};

export { getPoemByID, getRandomPoem, queryPoems, sendPoemComment };
