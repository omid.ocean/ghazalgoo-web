import { initI18n } from './i18n';
import { initIcons } from './icons';
const config = () => {
  initI18n();
  initIcons();
};
export { config };
