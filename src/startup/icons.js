import { library } from '@fortawesome/fontawesome-svg-core';
import { faTelegram, faGoogle } from '@fortawesome/free-brands-svg-icons';
import {
  faSearch,
  faStar,
  faBookOpen,
  faTimes,
  faFileAlt,
  faThumbsUp
} from '@fortawesome/free-solid-svg-icons';

const initIcons = () => {
  library.add(
    faTelegram,
    faGoogle,
    faSearch,
    faStar,
    faBookOpen,
    faTimes,
    faFileAlt,
    faThumbsUp
  );
};

export { initIcons };
