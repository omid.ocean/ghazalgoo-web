import i18n from 'i18next';
import { reactI18nextModule } from 'react-i18next';
import translationEN from '../locales/en/translation.json';
import translationFA from '../locales/fa/translation.json';

const resources = {
  en: {
    translation: translationEN
  },
  fa: {
    translation: translationFA
  }
};

const initI18n = () => {
  i18n
    .use(reactI18nextModule) // passes i18n down to react-i18next
    .init({
      interpolation: {
        escapeValue: false // react already safes from xss
      },
      lng: 'fa',

      resources
    });
};

export { initI18n, i18n };
