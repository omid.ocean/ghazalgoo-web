import {
  GET_POEM,
  GET_RANDOM_POEM,
  REMOVE_POEM,
  SEARCH_POEM,
  COMMENT_ADDED
} from '../actions/types';

const initialState = {
  poemFull: {},
  randomPoem: {},
  queryResults: {}
};

const poemReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMMENT_ADDED:
      return {
        ...state,
        poemFull: {
          ...state.poemFull,
          comments: [...state.poemFull.comments, action.payload]
        }
      };
    case SEARCH_POEM:
      return {
        ...state,
        queryResults: action.payload
      };
    case REMOVE_POEM:
      return {
        ...state,
        poemFull: {}
      };
    case GET_RANDOM_POEM:
      return {
        ...state,
        randomPoem: action.payload
      };
    case GET_POEM:
      return {
        ...state,
        poemFull: action.payload
      };
    default:
      return state;
  }
};

export { poemReducer };
