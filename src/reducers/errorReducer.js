import { GET_ERRORS, COMMENT_ADDED } from '../actions/types';

const initialState = {};

const errorReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMMENT_ADDED:
      return initialState;
    case GET_ERRORS:
      return action.payload;
    default:
      return state;
  }
};

export { errorReducer };
