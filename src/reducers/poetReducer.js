import { GET_POET, GET_POETS_LIST, REMOVE_POET } from '../actions/types';

const initialState = {
  poetFull: {},
  poetList: {}
};

const poetReducer = (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_POET:
      return {
        ...state,
        poetFull: {}
      };
    case GET_POETS_LIST:
      return {
        ...state,
        poetList: action.payload
      };
    case GET_POET:
      return {
        ...state,
        poetFull: action.payload
      };
    default:
      return state;
  }
};

export { poetReducer };
