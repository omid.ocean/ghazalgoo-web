import {
  GET_CATEGORY,
  SET_CURRENT_TOP_CAT,
  REMOVE_POET
} from '../actions/types';

const initialState = {
  categoryFull: {},
  currentTopCat: null
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_POET:
      return initialState;
    case SET_CURRENT_TOP_CAT:
      return {
        ...state,
        currentTopCat: action.payload
      };
    case GET_CATEGORY:
      return {
        ...state,
        categoryFull: action.payload,
        currentTopCat: action.payload._id
      };
    default:
      return state;
  }
};

export { categoryReducer };
