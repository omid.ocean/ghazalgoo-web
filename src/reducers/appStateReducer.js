import {
  REGISTER_USER,
  CLOSE_POPUP,
  OPEN_POPUP,
  CHANGE_PAGE,
  CHANGE_LANGUAGE
} from '../actions/types';

const initialState = {
  language: 'fa',
  popup: false,
  popupPage: 'login'
};

const appStateReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_USER:
      return {
        ...state,
        popupPage: 'login'
      };
    case CLOSE_POPUP:
      return {
        ...state,
        popup: false
      };
    case OPEN_POPUP:
      return {
        ...state,
        popup: true
      };

    case CHANGE_PAGE:
      return {
        ...state,
        popup: true,
        popupPage: action.payload
      };

    case CHANGE_LANGUAGE:
      return {
        ...state,
        language: action.payload
      };
    default:
      return state;
  }
};

export { appStateReducer };
