import { combineReducers } from 'redux';
import { appStateReducer } from './appStateReducer';
import { authReducer } from './authReducer';
import { errorReducer } from './errorReducer';
import { poetReducer } from './poetReducer';
import { categoryReducer } from './categoryReducer';
import { poemReducer } from './poemReducer';

export default combineReducers({
  auth: authReducer,
  appState: appStateReducer,
  error: errorReducer,
  poet: poetReducer,
  category: categoryReducer,
  poem: poemReducer
});
